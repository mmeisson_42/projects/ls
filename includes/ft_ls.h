/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ls.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/23 15:53:11 by mmeisson          #+#    #+#             */
/*   Updated: 2016/07/23 17:02:54 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_LS_H
# define FT_LS_H

# include "libft.h"
# include <stdio.h>
# include <dirent.h>
# include <sys/types.h>
# include <sys/stat.h>
# include <pwd.h>
# include <uuid/uuid.h>
# include <grp.h>
# include <sys/ioctl.h>
# include <time.h>
# include <errno.h>
# include <sys/acl.h>
# include <sys/xattr.h>

# define OOM "System error : Out Of Memory"
# define SIX_MONTHS 15811200

# define S_MEMALLOC(var, sz) if (!(var = ft_memalloc(sz))) ft_exit("OOM")

typedef struct		s_link
{
	struct s_link	*next;
}					t_link;

typedef struct		s_file
{
	struct s_file	*next;
	char			d_name[256];
	char			*f_name;
	struct stat		*s_stat;
}					t_file;

typedef struct		s_dir
{
	struct s_dir	*next;
	DIR				*content;
	char			*path;
}					t_dir;

typedef struct		s_size
{
	int		nb_blocks;
	int		size_block;
	int		list_size;
	int		nb_row;
}					t_size;

typedef struct		s_buff_stat
{
	size_t				s_sym_links;
	size_t				s_user;
	size_t				s_group;
	size_t				s_size;
	size_t				s_date;
	size_t				s_major;
	size_t				s_minor;
	size_t				s_name;
	char				rights[16];
	char				sym_links[8];
	char				user[64];
	char				group[64];
	char				size[20];
	char				date[28];
	char				name[520];
	char				major[8];
	char				minor[8];
	struct s_buff_stat	*next;
}					t_buff_stat;

typedef struct		s_stat_size
{
	size_t			sym_links;
	size_t			user;
	size_t			group;
	size_t			size;
	size_t			date;
	size_t			major;
	size_t			minor;
	size_t			name;
	int				total_blocks;
}					t_stat_size;

void				ft_dir_controller(t_dir *list_dir, char *options);
t_list				*ft_ls_parser(int ac, char **av, char **options);

void				ft_warning(char *message);
void				ft_warning_clean(char *message);
void				ft_exit_option(char e);
void				ft_exit(char *message);

void				get_sym(t_buff_stat *b_stat, t_stat_size *size,
		struct stat *s_stat);
void				get_user(t_buff_stat *b_stat, t_stat_size *size,
		struct stat *s_stat);
void				get_group(t_buff_stat *b_stat, t_stat_size *size,
		struct stat *s_stat);
void				get_size(t_buff_stat *b_stat, t_stat_size *size,
		struct stat *s_stat);
void				get_date(t_buff_stat *b_stat, t_stat_size *size,
		struct stat *s_stat, char *options);
void				get_dev(t_buff_stat *b_stat, t_stat_size *size,
		struct stat *s_stat);
void				get_name(t_buff_stat *b_stat, t_file *file,
		char *options);
void				get_rights(char (*buffer)[], struct stat *s_stat,
		char *path);
void				get_type(char (*buffer)[], struct stat *s_stat);
char				*get_managed_time(time_t my_time, char *options);

void				ft_resize(t_buff_stat *buffer, t_stat_size *size_stat);

void				ft_sort_file(t_file **list, char *options);
void				ft_sort_list(t_list **list, char *options);
t_link				*merge_list(t_link *list, char *options, long (*cmp)());
t_file				*ft_reverse_file(t_file **displ);
long				ft_file_name(t_file *l1, t_file *l2, char *options);
long				ft_list_name(t_list *l1, t_list *l2, char *options);
long				ft_file_time(t_file *l1, t_file *l2, char *options);
long				ft_list_time(t_list *l1, t_list *l2, char *options);

void				ft_default_display(t_file *displays, char *options);
void				ft_long_display(t_file *displays, char *options,
		char *title);
void				ft_display_manager(t_file **displays, char *options,
		char *title, int tem);

t_dir				*ft_dirnew(DIR **dir, char *path);
t_file				*ft_filenew(char name[], char *content);
void				ft_deldir(t_dir **dir);
void				ft_delfile(t_file **file);
void				ft_insertback(void *struc, void *node);
void				ft_insertfront(void *struc, void *node);

char				*ft_itostr(int nb, char *str);
size_t				ft_strnextcat(char *dst, char *src, size_t tem,
		size_t size);
void				ft_cleanmove(t_buff_stat **b_stat);

#endif
