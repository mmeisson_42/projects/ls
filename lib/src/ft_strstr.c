/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/23 11:04:31 by mmeisson          #+#    #+#             */
/*   Updated: 2015/12/09 15:58:45 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strstr(const char *haystack, const char *needle)
{
	int		i;
	char	*tmp;

	tmp = (char*)haystack;
	if (!*needle)
		return (tmp);
	while (*tmp)
	{
		i = 0;
		while (tmp[i] == needle[i])
		{
			if (!needle[i])
				return (tmp);
			i++;
		}
		if (!needle[i])
			return (tmp);
		tmp++;
	}
	return (NULL);
}
