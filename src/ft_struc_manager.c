/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_struct_manager.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/17 13:03:40 by mmeisson          #+#    #+#             */
/*   Updated: 2016/07/23 17:03:01 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

t_dir			*ft_dirnew(DIR **dir, char *path)
{
	t_dir		*node;

	S_MEMALLOC(node, sizeof(t_dir));
	if (dir && *dir)
	{
		if (!(node->content = ft_memalloc(sizeof(DIR*))))
			ft_exit(OOM);
		else
			ft_memcpy(node->content, dir, sizeof(DIR*));
	}
	if (path[strlen(path) - 1] == '/' &&
			!(node->path = ft_strdup(path)))
		ft_exit(OOM);
	else if (!(node->path = ft_strjoin(path, "/")))
		ft_exit(OOM);
	return (node);
}

t_file			*ft_filenew(char name[], char *path)
{
	t_file		*node;
	size_t		i;
	size_t		j;

	S_MEMALLOC(node, sizeof(t_file));
	ft_strcpy(node->d_name, name);
	if (path)
	{
		i = ft_strlen(name);
		j = ft_strlen(path);
		S_MEMALLOC(node->f_name, sizeof(char) * (3 + i + j));
		ft_strcpy(node->f_name, path);
		if ((node->f_name)[j - 1] != '/')
			ft_strcpy((node->f_name) + j++, "/");
		ft_strcpy((node->f_name) + j, name);
	}
	else if (!(node->f_name = ft_strdup(name)))
		ft_exit(OOM);
	S_MEMALLOC(node->s_stat, sizeof(struct stat));
	if (lstat(node->f_name, node->s_stat) != -1)
		return (node);
	free(node->s_stat);
	node->s_stat = NULL;
	ft_warning(name);
	return (NULL);
}

void			ft_deldir(t_dir **dir)
{
	if (!*dir)
		return ;
	ft_deldir(&((*dir)->next));
	if ((*dir)->path)
		free((*dir)->path);
	if ((*dir)->content)
	{
		closedir(*((DIR**)((*dir)->content)));
		free((*dir)->content);
	}
	free(*dir);
	*dir = NULL;
}

void			ft_delfile(t_file **file)
{
	if (!*file)
		return ;
	ft_delfile(&((*file)->next));
	if ((*file)->f_name)
		free((*file)->f_name);
	if ((*file)->s_stat)
		free((*file)->s_stat);
	free(*file);
	*file = NULL;
}
