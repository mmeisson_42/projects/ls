/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sort_manager.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/13 14:51:24 by mmeisson          #+#    #+#             */
/*   Updated: 2016/07/23 17:03:01 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

void			ft_sort_file(t_file **list, char *options)
{
	static long		(*cmp)() = NULL;

	if (!cmp)
		cmp = ft_strchr(options, 't') ? ft_file_time : ft_file_name;
	*list = (t_file*)merge_list((t_link*)(*list), options, cmp);
}

void			ft_sort_list(t_list **list, char *options)
{
	static long		(*cmp)() = NULL;

	if (!cmp)
		cmp = ft_strchr(options, 't') ? ft_list_time : ft_list_name;
	*list = (t_list*)merge_list((t_link*)(*list), options, cmp);
}
