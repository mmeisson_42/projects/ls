/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/13 11:56:33 by mmeisson          #+#    #+#             */
/*   Updated: 2016/07/23 17:03:01 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

void			ft_lstdelall(t_list **list)
{
	if (!*list)
		return ;
	ft_lstdelall(&((*list)->next));
	free((*list)->content);
	free(*list);
	*list = NULL;
}

static void		ft_get_directory(t_dir **list_dir, t_list *tmp,
		char *options)
{
	DIR				*dir;
	static void		(*insert)(void*, void*) = NULL;

	if (!insert)
		insert = (ft_strchr(options, 'r')) ? ft_insertfront : ft_insertback;
	if ((dir = opendir((char*)tmp->content)))
		insert(list_dir, ft_dirnew(&dir, (char*)tmp->content));
	else if (errno == EACCES)
	{
		insert(list_dir, ft_dirnew(NULL, (char*)tmp->content));
		ft_warning((char*)tmp->content);
	}
	else
		ft_warning((char*)tmp->content);
}

static t_file	*ft_split_list(t_list *tmp, t_dir **list_dir,
		struct stat *s_stat, char *options)
{
	struct stat		sym_stat;
	char			buff[256];
	t_file			*file;

	ft_bzero(buff, 256);
	file = NULL;
	if (!ft_strchr(options, 'd') && S_ISDIR(s_stat->st_mode))
		ft_get_directory(list_dir, tmp, options);
	else if (!ft_strchr(options, 'l') && S_ISLNK(s_stat->st_mode) &&
			readlink((char*)tmp->content, buff, 255) != -1 &&
			lstat(buff, &sym_stat) != -1 && S_ISDIR(sym_stat.st_mode))
		ft_get_directory(list_dir, tmp, options);
	else
		file = ft_filenew((char*)tmp->content, NULL);
	return (file);
}

static void		ft_first_display(t_list *buffer, t_dir **list_dir,
		char *options)
{
	struct stat		s_stat;
	t_file			*list_files;
	t_list			*tmp;
	t_file			*file;

	tmp = buffer;
	list_files = NULL;
	file = NULL;
	while (tmp)
	{
		if (!*((char*)tmp->content))
			ft_exit("fts_open: No such file or directory");
		if (lstat((char*)tmp->content, &s_stat) == -1)
			ft_warning((char*)tmp->content);
		else
		{
			if ((file = ft_split_list(tmp, list_dir, &s_stat, options)))
				ft_insertback(&list_files, file);
		}
		tmp = tmp->next;
	}
	if (list_files && options)
		ft_display_manager(&list_files, options, NULL, 0);
	ft_delfile(&list_files);
}

int				main(int ac, char **av)
{
	char			*options;
	t_list			*buffer;
	t_dir			*list_dir;

	options = NULL;
	list_dir = NULL;
	buffer = ft_ls_parser(ac, av, &options);
	ft_sort_list(&buffer, options);
	ft_first_display(buffer, &list_dir, options);
	ft_lstdelall(&buffer);
	ft_dir_controller(list_dir, options);
	free(options);
	return (0);
}
