/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_format_long.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/04 17:21:39 by mmeisson          #+#    #+#             */
/*   Updated: 2016/07/23 17:03:00 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

void				ft_size(t_buff_stat *buffer, t_stat_size *size_stat)
{
	size_t		size;

	size = size_stat->size - buffer->s_size;
	if (size)
	{
		ft_memmove((buffer->size) + size, (buffer->size), buffer->s_size);
		ft_memset(buffer->size, ' ', size);
	}
}

void				ft_memspace(char *str, size_t str_len, size_t size)
{
	ft_memmove(str + size, str, str_len);
	ft_memset(str, ' ', size);
}

void				ft_major_size(t_buff_stat *buffer, t_stat_size *size_stat)
{
	size_t		size;

	if (size_stat->major)
	{
		if (buffer->s_major || buffer->s_minor)
		{
			size = size_stat->major - buffer->s_major;
			if (size)
				ft_memspace(buffer->major, buffer->s_major, size);
			size = size_stat->minor - buffer->s_minor;
			if (size)
				ft_memspace(buffer->minor, buffer->s_minor, size);
		}
		else
		{
			size = size_stat->major + size_stat->minor - buffer->s_size;
			if (size)
				ft_memspace(buffer->size, buffer->s_size, size);
		}
	}
	else
		ft_size(buffer, size_stat);
}

void				ft_resize(t_buff_stat *buffer, t_stat_size *size_stat)
{
	size_t	i;

	while (buffer)
	{
		i = size_stat->sym_links - buffer->s_sym_links;
		if (i)
			ft_memspace(buffer->sym_links, buffer->s_sym_links, i);
		i = size_stat->user - buffer->s_user;
		if (i)
			ft_memset(buffer->user + buffer->s_user, ' ', i);
		i = size_stat->group - buffer->s_group;
		if (i)
			ft_memset(buffer->group + buffer->s_group, ' ', i);
		ft_major_size(buffer, size_stat);
		buffer = buffer->next;
	}
}
