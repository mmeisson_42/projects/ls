/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_display_manager.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/13 15:28:22 by mmeisson          #+#    #+#             */
/*   Updated: 2016/07/23 17:03:00 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

void		get_type(char (*buffer)[], struct stat *s_stat)
{
	if (S_ISREG(s_stat->st_mode))
		(*buffer)[0] = '-';
	else if (S_ISDIR(s_stat->st_mode))
		(*buffer)[0] = 'd';
	else if (S_ISCHR(s_stat->st_mode))
		(*buffer)[0] = 'c';
	else if (S_ISBLK(s_stat->st_mode))
		(*buffer)[0] = 'b';
	else if (S_ISLNK(s_stat->st_mode))
		(*buffer)[0] = 'l';
	else if (S_ISSOCK(s_stat->st_mode))
		(*buffer)[0] = 's';
	else if (S_ISFIFO(s_stat->st_mode))
		(*buffer)[0] = 'p';
	else
		(*buffer)[0] = '?';
}

void		get_acl_extends(char (*buffer)[], struct stat *s_stat, char *path)
{
	acl_t	acl;
	char	buf[255];

	if (s_stat->st_mode & S_ISUID)
		(*buffer)[3] = (s_stat->st_mode & S_IXUSR) ? 's' : 'S';
	if (s_stat->st_mode & S_ISGID)
		(*buffer)[6] = (s_stat->st_mode & S_IXGRP) ? 's' : 'S';
	if (s_stat->st_mode & S_ISVTX)
		(*buffer)[9] = (s_stat->st_mode & S_IXUSR) ? 't' : 'T';
	if (listxattr(path, buf, 255, XATTR_NOFOLLOW) > 0)
		(*buffer)[10] = '@';
	else if ((acl = acl_get_file(path, ACL_TYPE_EXTENDED)))
	{
		free(acl);
		(*buffer)[10] = '+';
	}
	else
		(*buffer)[10] = ' ';
}

void		get_rights(char (*buffer)[], struct stat *s_stat, char *path)
{
	(*buffer)[1] = (s_stat->st_mode & (1 << 8)) ? 'r' : '-';
	(*buffer)[2] = (s_stat->st_mode & (1 << 7)) ? 'w' : '-';
	(*buffer)[3] = (s_stat->st_mode & (1 << 6)) ? 'x' : '-';
	(*buffer)[4] = (s_stat->st_mode & (1 << 5)) ? 'r' : '-';
	(*buffer)[5] = (s_stat->st_mode & (1 << 4)) ? 'w' : '-';
	(*buffer)[6] = (s_stat->st_mode & (1 << 3)) ? 'x' : '-';
	(*buffer)[7] = (s_stat->st_mode & (1 << 2)) ? 'r' : '-';
	(*buffer)[8] = (s_stat->st_mode & (1 << 1)) ? 'w' : '-';
	(*buffer)[9] = (s_stat->st_mode & (1 << 0)) ? 'x' : '-';
	get_acl_extends(buffer, s_stat, path);
	(*buffer)[11] = ' ';
}

static void	ft_display_title(char *title, int tem_next)
{
	static int		tem_stat = 0;
	static int		tem = 0;

	tem_stat += tem_next;
	if (tem_stat++ && title)
	{
		if (tem)
			ft_putchar('\n');
		title[ft_strlen(title) - 1] = '\0';
		ft_putstr(title);
		ft_putendl(":");
	}
	tem++;
}

void		ft_display_manager(t_file **displ, char *options, char *title,
		int tem_next)
{
	int				i;
	t_file			*displays;

	i = 0;
	displays = (*displ && ft_strchr(options, 'r')) ?
		ft_reverse_file(displ) : *displ;
	ft_display_title(title, tem_next);
	if (ft_strchr(options, 'l'))
		ft_long_display(displays, options, title);
	else if (ft_strchr(options, '1'))
		while (displays)
		{
			if (ft_strchr(options, 'p') && S_ISDIR(displays->s_stat->st_mode))
			{
				ft_putstr((char*)displays->d_name);
				ft_putendl("/");
			}
			else
				ft_putendl((char*)displays->d_name);
			displays = displays->next;
		}
	else
		ft_default_display(displays, options);
}
