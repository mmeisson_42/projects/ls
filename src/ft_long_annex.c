/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_long_annex.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/14 11:51:13 by mmeisson          #+#    #+#             */
/*   Updated: 2016/07/23 17:03:01 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

void		get_sym(t_buff_stat *b_stat, t_stat_size *size_stat,
		struct stat *s_stat)
{
	size_t	len;

	ft_itostr((int)s_stat->st_nlink, b_stat->sym_links);
	len = ft_strlen(b_stat->sym_links);
	b_stat->sym_links[len++] = ' ';
	b_stat->s_sym_links = len;
	if (size_stat->sym_links < len)
		size_stat->sym_links = len;
}

void		get_size(t_buff_stat *b_stat, t_stat_size *size_stat,
		struct stat *s_stat)
{
	size_t	len;

	ft_itostr((int)s_stat->st_size, b_stat->size);
	len = ft_strlen(b_stat->size);
	(b_stat->size)[len++] = ' ';
	if (size_stat->size < len)
		size_stat->size = len;
	b_stat->s_size = len;
	size_stat->total_blocks += (int)s_stat->st_blocks;
}

void		get_user(t_buff_stat *b_stat, t_stat_size *size_stat,
		struct stat *s_stat)
{
	struct passwd		*pwd;
	size_t				len;

	len = 0;
	pwd = getpwuid(s_stat->st_uid);
	if (pwd)
	{
		len = ft_strlen(pwd->pw_name);
		ft_strcpy(b_stat->user, pwd->pw_name);
	}
	else
	{
		ft_itostr((int)s_stat->st_uid, b_stat->user);
		len = ft_strlen(b_stat->user);
	}
	(b_stat->user)[len++] = ' ';
	(b_stat->user)[len++] = ' ';
	b_stat->s_user = len;
	if (size_stat->user < len)
		size_stat->user = len;
}

void		get_group(t_buff_stat *b_stat, t_stat_size *size_stat,
		struct stat *s_stat)
{
	struct group		*grp;
	size_t				len;

	len = 0;
	grp = getgrgid(s_stat->st_gid);
	if (grp)
	{
		len = ft_strlen(grp->gr_name);
		ft_strcpy(b_stat->group, grp->gr_name);
	}
	else
	{
		ft_itostr((int)s_stat->st_uid, b_stat->group);
		len = ft_strlen(b_stat->group);
	}
	(b_stat->group)[len++] = ' ';
	(b_stat->group)[len++] = ' ';
	b_stat->s_group = len;
	if (size_stat->group < len)
		size_stat->group = len;
}

void		get_dev(t_buff_stat *b_stat, t_stat_size *size_stat,
			struct stat *s_stat)
{
	size_t		len;

	(b_stat->major)[0] = ' ';
	if (S_ISCHR(s_stat->st_mode) || S_ISBLK(s_stat->st_mode))
	{
		ft_itostr((int)major(s_stat->st_rdev), b_stat->major + 1);
		len = ft_strlen(b_stat->major);
		(b_stat->major)[len++] = ',';
		(b_stat->major)[len++] = ' ';
	}
	else
	{
		(b_stat->major)[0] = ' ';
		len = 1;
	}
	b_stat->s_major = len;
	if (len > size_stat->major)
		size_stat->major = len;
	ft_itostr((int)(s_stat->st_rdev % 256), b_stat->minor);
	len = ft_strlen(b_stat->minor);
	(b_stat->minor)[len++] = ' ';
	b_stat->s_minor = len;
	size_stat->minor = (len > size_stat->minor) ? len : size_stat->minor;
	if (size_stat->major + size_stat->minor > size_stat->size)
		size_stat->size = size_stat->major + size_stat->minor;
}
