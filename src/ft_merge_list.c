/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_merge_list.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/03 11:21:40 by mmeisson          #+#    #+#             */
/*   Updated: 2016/07/23 17:03:01 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

void		manage_insertion(t_link **node, t_link **head, t_link **tail)
{
	t_link	*prev;

	prev = *node;
	*node = prev->next;
	prev->next = NULL;
	if (*tail)
	{
		(*tail)->next = prev;
		*tail = (*tail)->next;
	}
	else
	{
		*head = prev;
		*tail = prev;
	}
}

t_link		*insertion_merge(t_link *slow, t_link *fast,
		char *options, long (*cmp)())
{
	t_link	*head;
	t_link	*tail;

	head = NULL;
	tail = NULL;
	while (slow && fast)
	{
		if (cmp(slow, fast, options) < 0)
			manage_insertion(&slow, &head, &tail);
		else
			manage_insertion(&fast, &head, &tail);
	}
	while (slow)
		manage_insertion(&slow, &head, &tail);
	while (fast)
		manage_insertion(&fast, &head, &tail);
	return (head);
}

t_link		*merge_list(t_link *list, char *options, long (*cmp)())
{
	t_link	*slow;
	t_link	*fast;
	t_link	*prev;

	if (list && list->next)
	{
		slow = list;
		fast = list->next;
		prev = slow;
		while (fast && fast->next)
		{
			prev = slow;
			slow = slow->next;
			fast = fast->next->next;
		}
		prev = slow;
		slow = slow->next;
		prev->next = NULL;
	}
	else
		return (list);
	fast = merge_list(slow, options, cmp);
	slow = merge_list(list, options, cmp);
	return (insertion_merge(slow, fast, options, cmp));
}
