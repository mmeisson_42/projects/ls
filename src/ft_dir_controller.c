/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_dir_controller.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/13 13:50:57 by mmeisson          #+#    #+#             */
/*   Updated: 2016/07/23 17:03:00 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

static t_dir		*ft_clean_files(t_file *files, char *options)
{
	t_dir			*list_dir;
	DIR				*dir;
	void			(*insert)(void*, void*);

	list_dir = NULL;
	insert = (ft_strchr(options, 'r')) ? ft_insertfront : ft_insertback;
	while (files)
	{
		if (ft_strcmp(files->d_name, ".") && ft_strcmp(files->d_name, ".."))
			if (S_ISDIR(files->s_stat->st_mode))
			{
				if ((dir = opendir(files->f_name)))
					insert(&list_dir, ft_dirnew(&dir, files->f_name));
				else if (errno == EACCES)
				{
					ft_warning(files->f_name);
					insert(&list_dir, ft_dirnew(NULL, files->f_name));
				}
				else
					ft_warning_clean(files->d_name);
			}
		files = files->next;
	}
	return (list_dir);
}

static void			ft_load_dir(t_dir *n_dir, t_file **displ, char *options)
{
	struct dirent	*sd;

	if (n_dir->content)
	{
		while ((sd = readdir(*((DIR**)n_dir->content))))
		{
			if (ft_strchr(options, 'a') ||
					(ft_strchr(options, 'A') && ft_strcmp(".", sd->d_name)
					&& ft_strcmp("..", sd->d_name)) ||
					(char)*(sd->d_name) != '.')
				ft_insertfront(displ, ft_filenew(sd->d_name, n_dir->path));
		}
	}
	ft_sort_file(displ, options);
}

t_file				*ft_reverse_file(t_file **displ)
{
	t_file			*displays;
	t_file			*tmp;
	t_file			*list;

	list = NULL;
	displays = *displ;
	tmp = displays->next;
	while (tmp)
	{
		ft_insertfront(&list, displays);
		displays = tmp;
		tmp = tmp->next;
	}
	ft_insertfront(&list, displays);
	displays = list;
	*displ = list;
	return (displays);
}

void				ft_dir_controller(t_dir *list, char *options)
{
	t_dir			*nlst_dir;
	t_dir			*list_dir;
	t_file			*displ;
	static int		tem = 0;

	nlst_dir = NULL;
	list_dir = list;
	displ = NULL;
	while (list_dir)
	{
		ft_load_dir(list_dir, &displ, options);
		tem += (list_dir->next) ? 1 : 0;
		if (ft_strchr(options, 'R'))
		{
			nlst_dir = ft_clean_files(displ, options);
			ft_display_manager(&displ, options, list_dir->path, tem++);
			if (nlst_dir)
				ft_dir_controller(nlst_dir, options);
		}
		else
			ft_display_manager(&displ, options, list_dir->path, tem);
		ft_delfile(&displ);
		list_dir = list_dir->next;
	}
	ft_deldir(&list);
}
