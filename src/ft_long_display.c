/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_long_display.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/20 15:39:50 by mmeisson          #+#    #+#             */
/*   Updated: 2016/07/23 17:03:01 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

static t_buff_stat		*ft_bufferise_stat(t_file *file, t_stat_size *size_stat,
		char *options)
{
	t_buff_stat		*b_stat;

	S_MEMALLOC(b_stat, sizeof(t_buff_stat));
	get_type(&(b_stat->rights), file->s_stat);
	get_rights(&(b_stat->rights), file->s_stat, file->f_name);
	get_sym(b_stat, size_stat, file->s_stat);
	get_user(b_stat, size_stat, file->s_stat);
	get_group(b_stat, size_stat, file->s_stat);
	if (!ft_strncmp("/dev", file->f_name, 4) && !S_ISDIR(file->s_stat->st_mode))
		get_dev(b_stat, size_stat, file->s_stat);
	else
		get_size(b_stat, size_stat, file->s_stat);
	get_date(b_stat, size_stat, file->s_stat, options);
	get_name(b_stat, file, options);
	return (b_stat);
}

static void				print_total(t_stat_size *size, char *options,
		char *title)
{
	static int		is_d = -1;

	if (is_d == -1)
		is_d = (ft_strchr(options, 'd')) ? 1 : 0;
	if (!is_d && title)
	{
		ft_putstr("total ");
		ft_putnbr(size->total_blocks);
		ft_putchar('\n');
	}
}

static void				ft_flushstat(t_buff_stat *buffer,
		t_stat_size *size, char *title, char *options)
{
	static char		buff[800] = {0};
	int				tem;

	print_total(size, options, title);
	while (buffer)
	{
		tem = 0;
		tem = ft_strnextcat(buff, buffer->rights, tem, 12);
		tem = ft_strnextcat(buff, buffer->sym_links, tem, size->sym_links);
		tem = ft_strnextcat(buff, buffer->user, tem, size->user);
		tem = ft_strnextcat(buff, buffer->group, tem, size->group);
		if (buffer->s_major)
		{
			tem = ft_strnextcat(buff, buffer->major, tem, size->major);
			tem = ft_strnextcat(buff, buffer->minor, tem, size->minor);
		}
		else
			tem = ft_strnextcat(buff, buffer->size, tem, size->size);
		tem = ft_strnextcat(buff, buffer->date, tem, size->date);
		tem = ft_strnextcat(buff, buffer->name, tem, buffer->s_name);
		ft_putendl(buff);
		ft_cleanmove(&buffer);
	}
}

void					ft_insert_stat(t_buff_stat **head, t_buff_stat *node)
{
	static t_buff_stat *foot;

	if (*head)
	{
		foot->next = node;
		foot = node;
	}
	else
	{
		foot = node;
		*head = node;
	}
}

/*
** No title means that file is single
*/

void					ft_long_display(t_file *displays, char *options,
		char *title)
{
	t_buff_stat		*head_stat;
	t_stat_size		size_stat;
	t_file			*head;

	head_stat = NULL;
	ft_bzero(&size_stat, sizeof(t_stat_size));
	head = displays;
	while (displays)
	{
		ft_insert_stat(&head_stat,
				ft_bufferise_stat(displays, &size_stat, options));
		displays = displays->next;
	}
	if (head_stat)
	{
		ft_resize(head_stat, &size_stat);
		ft_flushstat(head_stat, &size_stat, title, options);
	}
}
