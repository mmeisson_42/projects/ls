/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parser.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/02 13:34:45 by mmeisson          #+#    #+#             */
/*   Updated: 2016/07/23 17:03:01 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

/*
** These masks are auto inclusive, if an option is found in a mask, then all
** mask's options are automaticaly going to be set
*/

static void		add_option(char *opts, char c)
{
	static size_t		len = 0;

	if (!ft_strchr(opts, c))
		opts[len++] = c;
}

/*
** Care with set_option and replace_mask, each calls the other and this could
** process to an infinite loop if masks are not properly setted
*/

static void		ft_set_option(char *opts, char c)
{
	char		masks[1][1];
	size_t		i;
	size_t		j;
	size_t		masks_size;

	i = 0;
	masks_size = 0;
	if (masks_size)
		while (i < 1)
		{
			if (ft_strchr(masks[i], c))
			{
				j = 0;
				while (masks[i][j])
					add_option(opts, masks[i][j++]);
				return ;
			}
			i++;
		}
	add_option(opts, c);
}

static int		ft_replace_mask(char *opts, char c)
{
	char	masks[2][10];
	size_t	i;
	size_t	j;

	ft_strcpy(masks[0], "1l");
	ft_strcpy(masks[1], "Uc");
	i = 0;
	while (i < 1)
	{
		if (ft_strchr(masks[i], c))
		{
			j = -1;
			while (opts[++j])
				if (ft_strchr(masks[i], opts[j]))
				{
					opts[j] = c;
					return (1);
				}
			add_option(opts, c);
			return (1);
		}
		i++;
	}
	return (0);
}

static char		*ft_parse_options(int ac, char **av, int *cpt)
{
	int		i;
	int		j;
	char	*opts;

	i = 1;
	j = 0;
	S_MEMALLOC(opts, sizeof(char) * 30);
	while (i < ac && av[i][0] == '-' && av[i][1])
	{
		j = 0;
		if (!ft_strcmp(av[i], "--") && ++i)
			break ;
		while (av[i][++j])
		{
			if (!ft_strchr("aAcdlprRtTU1", av[i][j]))
				ft_exit_option(av[i][j]);
			if (!ft_replace_mask(opts, av[i][j]))
				ft_set_option(opts, av[i][j]);
		}
		i++;
	}
	*cpt = i;
	return (opts);
}

t_list			*ft_ls_parser(int ac, char **av, char **options)
{
	t_list	*paths;
	int		i;
	int		o;

	i = 1;
	o = 0;
	paths = NULL;
	*options = ft_parse_options(ac, av, &i);
	if (i == ac)
		return (ft_lstnew(".", 2));
	while (i < ac)
	{
		ft_lstadd(&paths, ft_lstnew(av[i], sizeof(char) *
					ft_strlen(av[i]) + 1));
		i++;
	}
	return (paths);
}
