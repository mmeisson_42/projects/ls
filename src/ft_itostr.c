/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itostr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/05 15:16:26 by mmeisson          #+#    #+#             */
/*   Updated: 2016/07/23 17:03:01 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

static size_t	get_str_size(int n)
{
	size_t		o;
	size_t		i;

	i = (n >= 0) ? 1 : 2;
	o = (n > 0) ? (size_t)n : (size_t)-n;
	while (n > 9)
	{
		n /= 10;
		i++;
	}
	return (i);
}

char			*ft_itostr(int nb, char *str)
{
	size_t		i;
	size_t		o;

	i = get_str_size(nb);
	o = (nb > 0) ? (size_t)nb : (size_t)-nb;
	str[i--] = '\0';
	if (nb < 0)
		str[0] = '-';
	while (o > 9)
	{
		str[i--] = o % 10 + 48;
		o /= 10;
	}
	str[i] = o + 48;
	return (str);
}

size_t			ft_strnextcat(char *dst, char *src, size_t tem,
		size_t size)
{
	ft_strcpy(dst + tem, src);
	return (tem + size);
}

void			ft_cleanmove(t_buff_stat **b_stat)
{
	t_buff_stat		*tmp;

	tmp = *b_stat;
	*b_stat = tmp->next;
	free(tmp);
}
