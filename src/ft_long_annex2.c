/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_long_annex2.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/19 14:53:01 by mmeisson          #+#    #+#             */
/*   Updated: 2016/07/23 17:03:01 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

static time_t	load_time(struct stat *s_stat, char *options)
{
	if (ft_strchr(options, 'c'))
		return (s_stat->st_ctimespec.tv_sec);
	else if (ft_strchr(options, 'U'))
		return (s_stat->st_birthtimespec.tv_sec);
	else
		return (s_stat->st_mtimespec.tv_sec);
}

char			*get_managed_time(time_t my_time, char *options)
{
	char			*managed_time;
	time_t			curr_time;
	size_t			i;

	managed_time = ctime(&my_time);
	curr_time = time(&curr_time);
	ft_strcpy(managed_time, managed_time + 4);
	if (!ft_strchr(options, 'T'))
	{
		if (curr_time > my_time + SIX_MONTHS || curr_time < my_time)
		{
			i = 0;
			managed_time[7] = ' ';
			while (managed_time[16 + i] == ' ')
				i++;
			ft_strcpy(managed_time + 8, managed_time + 16 + i);
		}
		else
		{
			ft_strcpy(managed_time + 12, managed_time + 15);
			ft_strclr(managed_time + 12);
		}
	}
	return (managed_time);
}

void			get_date(t_buff_stat *b_stat, t_stat_size *size_stat,
		struct stat *s_stat, char *options)
{
	char			*time;
	size_t			len;
	static time_t	my_time = 0;

	my_time = load_time(s_stat, options);
	time = get_managed_time(my_time, options);
	len = 0;
	while (time[len] && time[len] != '\n')
	{
		(b_stat->date)[len] = time[len];
		len++;
	}
	(b_stat->date)[len++] = ' ';
	b_stat->s_date = len;
	if (size_stat->date < len)
		size_stat->date = len;
}

static void		get_link(char (*buffer)[], t_file *file, size_t *j)
{
	size_t		i;

	ft_strcpy(*buffer + *j, " -> ");
	*j += 4;
	if (!(i = readlink(file->f_name, *buffer + *j, 255 - *j)))
	{
		ft_warning(file->f_name);
		ft_bzero(buffer, 255 - *j);
	}
	else
		*j += i;
}

void			get_name(t_buff_stat *b_stat, t_file *file, char *options)
{
	size_t			i;
	size_t			j;

	j = 0;
	i = 0;
	while ((file->d_name)[i])
		(b_stat->name)[j++] = (file->d_name)[i++];
	if (S_ISLNK(file->s_stat->st_mode))
		get_link(&(b_stat->name), file, &j);
	if (ft_strchr(options, 'p') && S_ISDIR(file->s_stat->st_mode))
		(b_stat->name)[j] = '/';
	b_stat->s_name = j;
}
