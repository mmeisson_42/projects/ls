/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   error.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/08 12:56:01 by mmeisson          #+#    #+#             */
/*   Updated: 2016/07/23 17:03:00 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

void	ft_warning(char *message)
{
	ft_putstr_fd("ft_ls: ", 2);
	perror(message);
}

void	ft_warning_clean(char *message)
{
	ft_putstr_fd(message, 2);
	ft_putendl_fd(":", 2);
	ft_putstr_fd("ft_ls: ", 2);
	perror(message);
}

void	ft_exit(char *message)
{
	ft_putstr_fd("ft_ls: ", 2);
	ft_putendl_fd(message, 2);
	exit(EXIT_FAILURE);
}

void	ft_exit_option(char e)
{
	ft_putstr_fd("ft_ls: illegal option -- ", 2);
	ft_putchar_fd(e, 2);
	ft_putchar_fd('\n', 2);
	ft_putendl_fd("usage: ft_ls [-acdlprRtTU1] [file ...]", 2);
	exit(EXIT_FAILURE);
}
