/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_insert_void.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/04 18:19:35 by mmeisson          #+#    #+#             */
/*   Updated: 2016/07/23 17:03:00 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

/*
** for insert functions, the first void argue should be a **struct
*/

void			ft_insertback(void *pt_struc, void *node)
{
	t_link		**h_link;
	t_link		*tmp;

	if (!node)
		return ;
	h_link = (t_link**)pt_struc;
	if (!*h_link)
		*h_link = (t_link*)node;
	else
	{
		tmp = *h_link;
		while (tmp->next)
			tmp = tmp->next;
		tmp->next = (t_link*)node;
	}
}

void			ft_insertfront(void *pt_struc, void *node)
{
	t_link		**h_link;
	t_link		*link;

	if (!node)
		return ;
	h_link = (t_link**)pt_struc;
	link = (t_link*)node;
	link->next = *h_link;
	*h_link = link;
}
