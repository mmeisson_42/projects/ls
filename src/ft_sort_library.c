/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sort_library.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/04 17:16:54 by mmeisson          #+#    #+#             */
/*   Updated: 2016/07/23 17:03:01 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

long		ft_file_name(t_file *l1, t_file *l2, char *options)
{
	if (options)
		;
	return ((long)ft_strcmp(l1->d_name, l2->d_name));
}

long		ft_file_time(t_file *l1, t_file *l2, char *options)
{
	long				ret;

	if (ft_strchr(options, 'c'))
	{
		ret = l2->s_stat->st_ctimespec.tv_sec - l1->s_stat->st_ctimespec.tv_sec;
		if (ret == 0)
			ret = l2->s_stat->st_ctimespec.tv_nsec -
				l1->s_stat->st_ctimespec.tv_nsec;
	}
	else if (ft_strchr(options, 'U'))
	{
		ret = l2->s_stat->st_birthtimespec.tv_sec -
			l1->s_stat->st_birthtimespec.tv_sec;
		if (ret == 0)
			ret = l2->s_stat->st_birthtimespec.tv_nsec -
				l1->s_stat->st_birthtimespec.tv_nsec;
	}
	else
	{
		ret = l2->s_stat->st_mtimespec.tv_sec - l1->s_stat->st_mtimespec.tv_sec;
		if (ret == 0)
			ret = l2->s_stat->st_mtimespec.tv_nsec -
				l1->s_stat->st_mtimespec.tv_nsec;
	}
	return (ret);
}

long		ft_list_name(t_list *l1, t_list *l2, char *options)
{
	if (options)
		;
	return ((long)ft_strcmp((char*)l1->content, (char*)l2->content));
}

long		ft_list_time(t_list *l1, t_list *l2, char *options)
{
	long				ret;
	struct stat			s_1;
	struct stat			s_2;

	lstat((char*)l1->content, &s_1);
	lstat((char*)l2->content, &s_2);
	if (ft_strchr(options, 'c'))
		ret = s_2.st_ctimespec.tv_nsec - s_1.st_ctimespec.tv_sec;
	else if (ft_strchr(options, 'U'))
		ret = s_2.st_atimespec.tv_nsec - s_1.st_atimespec.tv_sec;
	else
		ret = s_2.st_mtimespec.tv_nsec - s_1.st_mtimespec.tv_sec;
	if (ret == 0)
		ret = ft_list_name(l1, l2, options);
	return (ret);
}
