/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_default_display.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/18 18:56:41 by mmeisson          #+#    #+#             */
/*   Updated: 2016/07/23 17:03:00 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

int				largest_node(t_file *displays, int *size)
{
	int		i;
	int		j;
	int		l_size;

	i = 0;
	l_size = 0;
	while (displays)
	{
		j = ft_strlen(displays->d_name);
		if (j > i)
			i = j;
		displays = displays->next;
		l_size++;
	}
	*size = l_size;
	return (i);
}

void			ft_strscpy(char *dst, char *src)
{
	while (*src && *src != '\n')
		*(dst++) = *(src++);
}

void			ft_serialise(char ***map, int **arr, t_size *sz,
		struct winsize *w)
{
	char	**str;
	int		*tab;
	int		i;

	i = -1;
	S_MEMALLOC(str, sizeof(char*) * sz->nb_row);
	S_MEMALLOC(tab, sizeof(int) * sz->nb_row);
	while (++i < sz->nb_row)
	{
		if (!(str[i] = ft_strnew(w->ws_col + 1)))
			ft_exit(OOM);
		ft_memset(str[i], ' ', sizeof(char) * (sz->size_block * sz->nb_blocks));
	}
	*arr = tab;
	*map = str;
}

static char		**ft_bufferise(t_file *displays, t_size *sz, struct winsize *w,
		int is_p)
{
	char		**str;
	char		*name;
	int			*tab;
	int			i;

	ft_serialise(&str, &tab, sz, w);
	i = 0;
	while (displays)
	{
		if (S_ISDIR(displays->s_stat->st_mode) && is_p)
			name = ft_strjoin(displays->d_name, "/");
		else
			name = ft_strdup(displays->d_name);
		ft_strscpy(&str[i][tab[i] * sz->size_block], name);
		free(name);
		tab[i]++;
		i = (++i == sz->nb_row) ? 0 : i;
		displays = displays->next;
	}
	i = -1;
	while (++i < sz->nb_row)
		str[i][tab[i] * sz->size_block] = '\0';
	free(tab);
	return (str);
}

void			ft_default_display(t_file *displays, char *options)
{
	char			**buffer;
	t_size			size;
	struct winsize	w;
	int				i;
	static int		is_p = -1;

	if (!displays)
		return ;
	if (is_p == -1)
		is_p = (ft_strchr(options, 'p')) ? 1 : 0;
	ioctl(STDOUT_FILENO, TIOCGWINSZ, &w);
	size.list_size = 0;
	size.size_block = (largest_node(displays, &(size.list_size)) / 8 * 8) + 8;
	size.nb_blocks = w.ws_col / size.size_block;
	size.nb_row = (size.list_size % size.nb_blocks) ?
		size.list_size / size.nb_blocks + 1 :
		size.list_size / size.nb_blocks;
	buffer = ft_bufferise(displays, &size, &w, is_p);
	i = -1;
	while (++i < size.nb_row)
	{
		ft_putendl(buffer[i]);
		free(buffer[i]);
	}
	free(buffer);
}
