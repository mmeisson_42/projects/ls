# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: rpiccolo <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2016/07/23 15:52:51 by rpiccolo          #+#    #+#              #
#    Updated: 2016/07/23 16:16:18 by rpiccolo         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

CC = clang

NAME = ft_ls

SRC_PATH = ./src/
SRC_NAME = ft_dir_controller.c ft_display_manager.c ft_error.c ft_long_annex.c\
			ft_parser.c ft_sort_manager.c main.c ft_struc_manager.c\
			ft_default_display.c ft_long_annex2.c ft_format_long.c\
			ft_long_display.c ft_merge_list.c ft_sort_library.c\
			ft_insert_void.c ft_itostr.c

OBJ_PATH = ./obj/
OBJ_NAME = $(SRC_NAME:.c=.o)

INC_PATH = ./includes/ ./lib/includes

CFLAGS = -Wall -Werror -Wextra

LIB_PATH = ./lib/

SRC = $(addprefix $(SRC_PATH),$(SRC_NAME))
OBJ = $(addprefix $(OBJ_PATH),$(OBJ_NAME))
INC = $(addprefix -I,$(INC_PATH))
LIB = $(addprefix -L,$(LIB_PATH))

LIB_NAME = -lft
LDFLAGS = $(LIB) $(LIB_NAME)


all: libft $(NAME)

$(NAME): $(OBJ)
	$(CC) -o $(NAME) $(OBJ) $(LDFLAGS)

$(OBJ_PATH)%.o: $(SRC_PATH)%.c
	@mkdir -p $(OBJ_PATH)
	$(CC) $(CFLAGS) $(INC) -o $@ -c $<

libft:
	make -C $(LIB_PATH)

libft_re:
	make -C $(LIB_PATH) re

re_all: libft_re re

clean:
	@rm -f $(OBJ)
	@rmdir $(OBJ_PATH) 2>/dev/null || echo "" > /dev/null
	make -C lib clean

fclean: clean
	@rm -f $(NAME)
	make -C lib fclean

re: fclean all

.PHONY: all libft clean fclean re
